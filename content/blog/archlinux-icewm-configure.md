+++
title = "在ArchLinux下Icewm的配置过程。"
date = 2023-03-10
[taxonomies]
    tags = ["Icewm", "GNU/Linux","Arch"]
+++
## 关于IceWM

这个桌面管理器很好用，轻量，但有基本功能，可以做一个性能消耗少的桌面环境使用。（他也可以使用主题来个性化）
## 推荐的终端模拟器

这里我推荐的是**Alacritty**，我也正在使用。不需要什么配置，我已经觉得很好看了。
## 壁纸设置

壁纸我推荐使用**Icewm**自带的`icewmbg`,而不是去使用`feh`。
### 手动改变壁纸（适用于自带壁纸的主题）

使用指令：
```bash
$ icewmbg -i $PATHTOTHEIMAGEFILE
```
### 设置默认壁纸（适用于没有壁纸的主题）

**警告，你可能会遇到:每个用户都要重新配置,带壁纸的主题需要单独配置**
但是，我们大部分人，想要一开机，就是先前设置好的壁纸。
所以，我们要把**Icewm**的`preferences`文件找出来，我们先找到默认的配置文件夹。

```bash
$ whereis icewm
```

这样**Icewm**的目录在我的电脑上是`/usr/share/icewm/`。
把它目录下的`preferences`复制到`~/.icewm/`下。如果提示没有这个目录，你切换一下主题，他就会自动生成这个目录。
用文本编辑器打开，并迅速翻到最后一行，再往上找到这个样子的两行：
```bash
#  Desktop background images(s)
# DesktopBackgroundImage=""
```
把它改成这个样子：
```bash
#  Desktop background images(s)
DesktopBackgroundImage="the_path_to_image"
```
重启以后，选择一个默认没有壁纸的主题，你就会发现，你设置的默认图片生效了！
## 中文输入法

**中英文切换的默认按键是`Ctrl+Shift`,我测试重启后会有图标在任务栏上。**
这里我使用的是**Fcitx5**这个输入法，只要稍微修改一下`~/.xinitrc`就可以用了。
内容如下：
```bash
#设置中文环境变量
LANGUAGE=zh_CN
LANG=zh_CN
LC_ALL=zh_CN
LC_CTYPE=zh_CN
#设置系统使用的输入法
XMODIFIERS=@im=fcitx
export LANGUAGE LANG LC_ALL LC_CTYPE XMODIFIERS
#启动输入法
fcitx &
#启动icewm(这里你有写就不用再写一遍了)
exec icewm-session
```