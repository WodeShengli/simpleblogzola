+++
title = "在VoidLinux的KDE下配置音频的艰辛历程。"
date = 2023-07-14
[taxonomies]
    tags = ["Void", "Pipewire","GNU/Linux"]
+++
> 本文已经过时，新的办法只要自启动pipewire,wireplumber,和pipewire-pulse就好。
## 前言

我想使用**Pipewire**，因为官方的**Handbook**的视角挺分散的，对着官方的**Handbook**弄了半天，终于发现了到底要配置什么。先不废话，先配置好，在慢慢道明原因。
### 安装软件包

运行下面的命令来安装配置需要的包。
```
$ sudo xbps-install -S alsa-utils alsa-tools pipewire alsa-pipewire
```
### 开始配置音频

#### 配置好服务

运行下面的命令来启用服务。
```bash
$ sudo ln -s /etc/sv/alsa /var/service
```
#### 启用pipewire对alsa的支持

运行下面的命令来启用：
```bash
$ sudo mkdir -p /etc/alsa/conf.d
$ sudo ln -s /usr/share/alsa/alsa.conf.d/50-pipewire.conf /etc/alsa/conf.d
$ sudo ln -s /usr/share/alsa/alsa.conf.d/99-pipewire-default.conf /etc/alsa/conf.d
```
#### 设置自启动脚本

在自己的用户目录下新建一个`autostartscript`目录，新建一个`audio.sh`脚本如下：
```bash
#!/bin/sh
pipewire & wireplumber
```
运行下面的命令，使脚本变得可执行。
```bash
$ chmod +x ./audio.sh
```
## 后记
其实这在带**systemd**的发行版会自动配置替换服务的，所以右下角的音量控制一直都是无设备，**Handbook**里我找不到怎么配置用户服务什么的。
所以要连接**alsa**来使用`alsamixer`来控制音量。
如果你发现了可以让**KDE**右下角的音量控制工作的方法，可以通过邮箱告诉我嘛？谢谢。qwq