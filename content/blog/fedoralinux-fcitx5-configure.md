+++
title = "在Fedora的KDE定制版的输入法问题。"
date = 2023-04-01
[taxonomies]
    tags = ["Fcitx5", "GNU/Linux","KDE"]
+++
## 前言

这次我从**Debian Testing**切换到了**Fedora 37**，但是我不喜欢使用**Gnome**~~（内存泄漏+崩溃过）~~，所以使用了**Fedora**的**KDE**定制版，可是我遇到了一些问题。
## 为什么我用的是ibus？
### 卸载Ibus

这可能是官方没注意到？**首先开始这一切前，请更新系统。**
但是老玩家都知道，**KDE Plasma**主要搭配的输入法是**Fcitx5**，现在我们要卸载**Ibus**：
```bash
$ sudo dnf autoremove ibus
```
这样，在托盘上退出ibus服务。
### 安装 Fcitx5

现在我们来安装**Fcitx5**：
```bash
$ sudo dnf install fcitx5 fcitx5-configtool fcitx5-chinese-addons kcm-fcitx5
```
然后，我们还要去设置一下环境变量：
我们先备份一遍环境变量先输入`cp ~/.bash.rc ~/.bashrc.bak`。
再输入`nano ~/.bashrc`在文件末尾添加：
```bash
export GTK_IM_MODULE=fcitx
export QT_IM_MODULE=fcitx
export XMODIFIERS=@im=fcitx
export SDL_IM_MODULE=fcitx
```
然后注销系统，重新登陆后在任意输入框按`Ctrl + Space`，如果出现候选则设置成功。
现在我可以用输入法输入中文了qwq。
