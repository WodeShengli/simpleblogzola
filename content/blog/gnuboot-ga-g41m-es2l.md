+++
title = "在技嘉GA-G41M-ES2L主板上安装GNU Boot"
date = 2024-05-01
[taxonomies]
    tags = ["GNUBoot", "FreeSoftware","GNU/Linux"]
+++
> 文章造成的一切损失作者不承担责任，本文经供参考，读者请注意发布时间和遵守原安装向导以免造成不必要的损失。
## 你需要编译flashprog

总而言之，这个主板上的某些芯片组没有被`flashrom`很好的支持，所以有些主板会刷写失败，但是你也许可以尝试编译使用一下`flashprog`。

需要的依赖：
1. `pkg-config` 用来寻找库依赖
2. pciutils开发库包(`pciutils-dev`/`libpci-dev`/`pciutils-devel`, 这取决于你的发行版或者系统)
3. zlib开发库包(`zlib1g-dev`/`zlib-devel`, 这取决于你的发行版或者系统)
4. libftdi 1.0开发库包(`libftdi1-dev`/`libftdi-dev`), 可选,用来支持各种外部FT2232SPI刷写器
5. libusb 1.0开发库包(`libusb-1.0-0-dev`), 对各种基于USB的编程器的可选支持
6. libjaylink开发库包(`libjaylink-dev`), 对J-LINK编程器的可选支持
7. libgpiod开发库包(`libgpiod-dev`), 对在Linux平台下GPIO bitbanging的可选支持。
8. 编译的必须，像`gcc`,`make`,`meson`(`build-essential`或者类似的基础linux开发者包)
9. `git` (下载源码的)

编译并安装：
``` bash
make
sudo make install
```
## 你需要的rom

进ftp服务器下载就可以了，下载后解压。
**不要选只有grub或者grubfirst的!**
这两个貌似不怎么正常工作（也许要重装系统？或者不支持lvm加密？）
这边建议选SeaBIOS withgrub2的版本。这个是可以正常启动你之前的Linux发行版的。
## 临时添加内核启动参数

进grub2的时候按`e`编辑启动项，在linux那行添加一个内核参数`iomem=relaxed`，由于是临时的，失败了重启会恢复到原来的配置。
## 刷写

只以下需要两个指令，每次升级都要两个都要执行一遍！因为主板的Dual BIOS技术是可以回朔BIOS的，但貌似这个功能仅限于专有的BIOS?
``` bash
sudo flashprog -p internal:dualbiosindex=0 -w gnuboot.rom
sudo flashprog -p internal:dualbiosindex=1 -w gnuboot.rom
```
## 重启

没错，在系统内点重启，从系统内重启的话会卡死在黑屏界面。稍等一会(我那时大概等了3分钟)断掉电源再开机即可。
## 成果
![GNUBootgrub2](/img/gnubootgrub.jpg "GNU Boot的grub2负载")
![GNUBootSeaBIOS](/img/gnubootseabios.jpg "GNU Boot的SeaBIOS负载")
