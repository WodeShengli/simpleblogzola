+++
title = "在ArchLinux直接全新装Wireplumber导致没声音的问题。"
date = 2023-07-15
[taxonomies]
    tags = ["Arch", "Pipewire","GNU/Linux"]
+++
## 起因

今天重装了**ArchLinux**装**KDE**的时候，给我弹出了选项，是使用`pipewire-media-session`还是`wireplumber`呢？
由于Pipewire官方弃用了`pipewire-media-session`,所以我果断选择`wireplumber`，但是进入桌面后，我却发现，没有声音。
与**VoidLinux**不一样，**ArchLinux**上的**KDE**可以丝滑切换，所以不多废话直接开始配置。

### 启用用户级服务

注意看，下面这串命令不要使用**sudo**
```bash
$ systemctl --user enable wireplumber.service
$ systemctl --user start wireplumber.service
```
这下你应该能听到音乐了！qwq

### 额外

如果你想用**Pipewire**替代**pulseaudio**,运行下面的命令：
```bash
$ sudo pacman -S pipewire-pulse
```
重启后就替换好了的！awa
