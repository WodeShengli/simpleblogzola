+++
title = "在Windows上使用Helix-editor进行Rust™编程。"
date = 2023-04-01
[taxonomies]
    tags = ["Helix", "Windows","Rust™"]
+++
## 先决条件

**Rust™**：这肯定是必须的
**Helix-editor**：这里不给链接，请去自行搜索。
**Rust-analyzer**：这里我们要自己去编译一份，当然你也可以去使用官方释出的二进制包。
## 开始编译rust-analyzer

### 条件

**Rust™**：这肯定是必须的
**Git**：用来克隆源码
（记得换一下`crates.io`的源，这样能节省很多时间。）
打开一个用来存放源代码的文件夹，并输入以下指令：
```powershell
#这个仓库可以换国内的来提升访问速度。 $指代不需要管理员权限。
$ git clone https://github.com/rust-lang/rust-analyzer.git
$ cd rust-analyzer
$ cargo xtask install --server
```
编译完成后，输入`rust-analyzer`，如果没反应那就说明安装成功。
## 配置Helix-editor

请在终端使用`hx`命令来启动**Helix-editor**来产生一个默认的配置文件目录！
退出为`:qa!`。(放弃所有改动并退出。)
`Win+R`输入`%Appdata%`找到`helix`文件夹，在里面新建`languages.toml`。
打开`languages.toml`，添加内容如下:
```toml
[[language]]
name = "rust"

[language.config]
checkOnSave = { command = "clippy" }
```
所有配置已完毕。现在我们来体验用`Helix-editor`进行Rust™编程的乐趣吧！
## 开始使用

**在Windows下我推荐的终端模拟器是WezTerm，其他终端貌似与Helix-editor有字体渲染问题。**[Text rendering error #5753](https://github.com/helix-editor/helix/issues/5753)
**由于Rust-analyzer只在`cargo`创建的项目下提供代码提示，所以单文件是没有代码提示的。**
用终端打开项目文件夹输入`hx --health`,进入程序后,`空格`然后按一下`F`就可以浏览项目目录。

